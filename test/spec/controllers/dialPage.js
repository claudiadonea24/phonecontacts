'use strict';

describe('Controller: DialPageCtrl', function () {

  beforeEach(module('phoneContactsApp'));

  var DialPageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DialPageCtrl = $controller('DialPageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DialPageCtrl.data.number).toBe('');
  });
});
