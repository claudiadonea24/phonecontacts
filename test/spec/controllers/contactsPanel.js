'use strict';

describe('Controller: ContactsPageCtrl', function () {

  // load the controller's module
  beforeEach(module('phoneContactsApp'));

  var ContactsPageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ContactsPageCtrl = $controller('ContactsPageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ContactsPageCtrl.contactsList.length).toBe(9);
  });
});
