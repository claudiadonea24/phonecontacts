'use strict';

/**
 * @ngdoc overview
 * @name phoneContactsApp
 * @description
 * # phoneContactsApp
 *
 * Main module of the application.
 */
angular
  .module('phoneContactsApp', [
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/contacts.html',
        controller: 'ContactsPageCtrl',
        controllerAs: 'contactsPage'
      })
      .when('/dial', {
        templateUrl: 'views/dial.html',
        controller: 'DialPageCtrl',
        controllerAs: 'dialPage'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
