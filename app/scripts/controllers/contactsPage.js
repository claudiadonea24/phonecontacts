'use strict';

/**
 * @ngdoc function
 * @name phoneContactsApp.controller:ContactsCtrl
 * @description
 * # ContactsCtrl
 * Controller of the phoneContactsApp
 */
angular.module('phoneContactsApp')
  .controller('ContactsPageCtrl', ['contactData','$scope', function (contactData, $scope) {
    	contactData.getData().then(function(response){
    		$scope.contactsList = contactData.objectToArray(response.data);
    	},
		function(response){
			console.warn(response);
		});
  }]);
