'use strict';

/**
 * @ngdoc function
 * @name phoneContactsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the phoneContactsApp
 */
angular.module('phoneContactsApp')
  .controller('DialPageCtrl',['$scope', function ($scope) {
    $scope.data = {
    	number : ''
    };
     $scope.validNumber = function () {
        if (!$scope.phoneForm.phoneNumber.$valid) {
        	console.log($scope.number);
        }
        else return;
    };

    $scope.insertNumber = function(char){
    	$scope.number = $scope.number + char;
    };

  }]);
