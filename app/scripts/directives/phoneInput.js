'use strict';
angular.module('phoneContactsApp')
.directive('phoneInput', function($filter, $browser){
	return {
		link: function($scope, $element, $attrs){
			var parseTel = function() {
				var value = $element.val().replace(/[^0-9]/g, '');
	        	$element.val($filter('tel')(value, false));
			} 

	        $element.bind('change', parseTel);
	            $element.bind('keydown', function(event) {
	                var key = event.keyCode;
	                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
	                // This lets us support copy and paste too
	                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
	                    return;
	                }
	                $browser.defer(parseTel); // Have to do this or changes don't get picked up properly
	            });
		}
	}
});