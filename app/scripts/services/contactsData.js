'use strict';

angular.module('phoneContactsApp')
	.factory('contactData', ['$http', function($http){
		var url = 'https://smartnumbers-test.firebaseio.com/users.json';
		var getData = function(){
			var promise = $http({
				  method: 'GET',
				  url: url
				});
			
			return promise;
		};

		var objectToArray = function(obj){
			return Object.keys(obj).map(function (key) {return obj[key];});
		};

		return {
			getData: getData,
			objectToArray: objectToArray
		};
			
	}]);